const io = require("socket.io")(4000, {
  cors: {
    origin: "*",
  },
});
const { v4: uuidv4 } = require("uuid");

console.log("listen on port 4000");
let alerts = [];
let users = [];

io.on("connection", (socket) => {
  console.log("someone connected");

  // login and register to server
  socket.on("loginUser", (user) => {
    console.log(
      `someone try to login with the name: ${JSON.parse(user).username}`
    );
    const userCapature = users.find(
      (userObject) => userObject.username === JSON.parse(user).username
    );

    if (userCapature !== undefined) {
      if (userCapature.password === JSON.parse(user).password) {
        socket.emit("userConfirmed", {
          id: userCapature.id,
          username: userCapature.username,
        });
        console.log(`login of user ${userCapature.username} has confirmed`);
      } else {
        socket.emit("passwordNotMatch");
        console.log(`password of user ${userCapature.username} not match`);
      }
    } else {
      const newUser = { id: uuidv4(), ...JSON.parse(user) };
      users.push(newUser);
      console.log(`user ${newUser.username} has added`);
      socket.emit("userConfirmed", {
        id: newUser.id,
        username: newUser.username,
      });
      console.log(`login of user ${newUser.username} has confirmed`);
    }
  });

  // JOIN SOCKET ROOM ON CONNECT
  socket.on("syncApp", (userId) => {
    socket.join(userId);
    console.log(`user has joined room:${userId}`);
    const allMissingAlerts = alerts.filter((alert) => alert.userId === userId);

    socket.emit("fetchUserAlertsIfExist", JSON.stringify(allMissingAlerts));
    console.log(`${userId} syncing alerts`);
  });

  // ADD ALERT FROM MOBILE AND WEB
  socket.on("createAlert", (alert) => {
    const newAlert = { id: uuidv4(), ...JSON.parse(alert) };
    alerts.push(newAlert);

    if (newAlert.from === "web") {
      io.in(newAlert.userId).emit("createdAlert", newAlert);
      console.log(
        `alert id: ${newAlert.id} of user id: ${newAlert.userId} has added from web`
      );
    } else {
      socket.broadcast.to(newAlert.userId).emit("createdAlert", newAlert);
      console.log(
        `alert id: ${newAlert.id} of user id: ${newAlert.userId} has added from mobile`
      );
    }
  });

  // delete alert from mobile and web
  socket.on("deleteAlert", (alert) => {
    const alertToDelete = JSON.parse(alert);
    alerts = alerts.filter(
      (alertObject) => alertObject.id !== alertToDelete.id
    );

    if (alertToDelete.from === "web") {
      io.in(alertToDelete.userId).emit("deletedAlert", alertToDelete);
      console.log(
        `alert id: ${alertToDelete.id} of user id: ${alertToDelete.userId} has deleted from web`
      );
    } else {
      socket.broadcast.to(alertToDelete.userId).emit("deletedAlert", alert);
      console.log(
        `alert id: ${alertToDelete.id} of user id: ${alertToDelete.userId} has deleted from mobile`
      );
    }
  });

  socket.on("updateAlert", (alert) => {
    const alertToEdit = JSON.parse(alert);
    const index = alerts.findIndex(
      (alertObject) => alertObject.id === alertToEdit.id
    );
    alerts[index] = alertToEdit;

    if (alertToEdit.from === "web") {
      io.in(alertToEdit.userId).emit("updatedAlert", alertToEdit);
      console.log(
        `alert id: ${alertToEdit.id} of user id: ${alertToEdit.userId} has updated from web`
      );
    } else {
      socket.broadcast.to(alertToEdit.userId).emit("updatedAlert", alert);
      console.log(
        `alert id: ${alertToEdit.id} of user id: ${alertToEdit.userId} has updated from mobile`
      );
    }
  });

  socket.on("updateUser", (name, userId) => {
    let UpdateUserResult = { result: false, msg: "username already exist" };
    if (
      users.find((userObject) => userObject.username === name) === undefined
    ) {
      let userToUpdate = users.find((userObject) => userObject.id === userId);

      userToUpdate.username = name;
      io.in(`${userId}`).emit("updatedUser", JSON.stringify(userToUpdate));

      UpdateUserResult = { result: true, msg: "succeed" };
      console.log(`user id: ${userId} has updated`);
    } else {
      console.log(`username ${name} is already taken`);
    }

    socket.emit("usernameChangeResult", JSON.stringify(UpdateUserResult));
  });

  socket.on("disconnect", () => {
    console.log("someone has disconnected");
  });
});
